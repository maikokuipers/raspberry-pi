import RPi.GPIO as GPIO
import picamera
import os
from omxplayer.player import OMXPlayer
from pathlib import Path

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_UP)

camera = picamera.PiCamera()
camera.resolution = (640,480)
camera.framerate = 90
stream = picamera.PiCameraCircularIO(camera, seconds=6)
camera.start_recording(stream, format='h264')

player = None
VIDEO_PATH = Path("motion.mp4")

try:
    while True:
        camera.wait_recording(0)
        if not GPIO.input(18):
            print "pushed the GPIO 18 button"
            os.system('rm motion.mp4')
            stream.copy_to('motion.h264')

            os.system('MP4Box -fps 30 -add motion.h264 motion.mp4')

            player = OMXPlayer(VIDEO_PATH)

        if not GPIO.input(4):
            print "pushed the GPIO 4 button"
            if player._process.poll() is None:
                player.seek(-1)
            else:
                player = OMXPlayer(VIDEO_PATH)
            camera.wait_recording(0.5)

finally:
    camera.stop_preview()
    camera.stop_recording()
    camera.close()